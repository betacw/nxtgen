-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 27, 2016 at 06:35 PM
-- Server version: 5.5.49-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+03:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `adhrccok_nxtgen`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(5) NOT NULL,
  `activity` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `activity`) VALUES
(1, 'Attach'),
(2, 'Detach');

-- --------------------------------------------------------

--
-- Table structure for table `api_user`
--

CREATE TABLE IF NOT EXISTS `api_user` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `api_key_id` int(5) NOT NULL,
  `user_type` int(2) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE IF NOT EXISTS `data` (
  `device_id` int(100) NOT NULL,
  `temperature` double NOT NULL,
  `pressure` double NOT NULL,
  `heartrate` double NOT NULL,
  `datetime` datetime NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`device_id`, `temperature`, `pressure`, `heartrate`, `datetime`, `status`) VALUES
(12, 37.6, 81, 71, '2016-07-10 18:23:46', 1),
(12, 37.6, 81, 71, '2016-07-10 18:24:11', 1),
(12, 37.6, 81, 71, '2016-07-10 18:24:13', 1),
(12, 37.6, 81, 71, '2016-07-10 18:26:53', 1),
(12, 37.6, 81, 71, '2016-07-10 18:26:55', 1),
(12, 37.6, 81, 71, '2016-07-10 18:26:56', 1),
(12, 37.6, 81, 71, '2016-07-10 18:26:56', 1),
(12, 37.6, 81, 71, '2016-07-10 18:26:57', 1),
(12, 37.6, 81, 71, '2016-07-10 18:26:58', 1),
(12, 37.6, 81, 71, '2016-07-10 18:27:48', 1),
(13, 37.6, 81, 71, '2016-07-10 19:17:44', 1),
(13, 37.6, 81, 71, '2016-07-10 19:17:45', 1),
(13, 37.6, 81, 71, '2016-07-10 19:17:46', 1),
(13, 37.6, 81, 71, '2016-07-10 19:17:47', 1),
(13, 37.6, 81, 71, '2016-07-10 19:17:47', 1),
(14, 37.6, 81, 71, '2016-07-10 19:17:53', 1),
(14, 37.6, 81, 71, '2016-07-10 19:17:54', 1),
(14, 37.6, 81, 71, '2016-07-10 19:17:54', 1),
(14, 37.6, 81, 71, '2016-07-10 19:17:55', 1),
(14, 37.6, 81, 71, '2016-07-10 19:17:56', 1),
(14, 37.6, 81, 71, '2016-07-10 19:17:57', 1),
(14, 37.6, 81, 71, '2016-07-10 19:44:43', 1),
(14, 37.6, 81, 71, '2016-07-10 19:44:55', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:01', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:04', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:05', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:07', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:08', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:09', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:11', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:13', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:14', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:16', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:21', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:23', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:24', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:26', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:27', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:29', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:31', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:37', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:39', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:43', 1),
(14, 37.6, 81, 71, '2016-07-10 19:45:45', 1),
(14, 37.6, 81, 71, '2016-07-10 19:47:01', 1),
(14, 37.6, 81, 71, '2016-07-10 20:05:00', 1),
(14, 37.6, 81, 71, '2016-07-10 20:05:07', 1),
(14, 37.6, 81, 71, '2016-07-10 20:08:38', 1),
(14, 37.6, 81, 71, '2016-07-10 20:08:42', 1),
(14, 37.6, 81, 71, '2016-07-10 20:08:44', 1),
(14, 37.6, 81, 71, '2016-07-10 20:08:45', 1),
(14, 37.6, 81, 71, '2016-07-10 20:08:47', 1),
(14, 37.6, 81, 71, '2016-07-10 20:08:48', 1),
(14, 37.6, 81, 71, '2016-07-10 20:10:42', 1),
(14, 37.6, 81, 71, '2016-07-10 20:10:47', 1),
(14, 37.6, 81, 71, '2016-07-10 20:11:42', 1),
(14, 37.6, 81, 71, '2016-07-10 20:18:40', 1),
(14, 37.6, 81, 71, '2016-07-11 07:02:27', 1),
(14, 37.6, 81, 71, '2016-07-11 07:04:21', 1),
(14, 37.6, 81, 71, '2016-07-11 07:04:24', 1),
(14, 37.6, 81, 71, '2016-07-11 07:04:25', 1),
(14, 37.6, 81, 71, '2016-07-11 07:04:26', 1),
(14, 37.6, 81, 71, '2016-07-11 07:04:27', 1),
(14, 37.6, 81, 71, '2016-07-11 07:05:14', 1),
(14, 37.6, 81, 71, '2016-07-11 07:05:18', 1),
(14, 0, 0, 0, '0000-00-00 00:00:00', 1),
(14, 0, 0, 0, '0000-00-00 00:00:00', 1),
(14, 0, 0, 0, '0000-00-00 00:00:00', 1),
(14, 0, 0, 0, '0000-00-00 00:00:00', 1),
(14, 0, 0, 0, '0000-00-00 00:00:00', 1),
(14, 0, 0, 0, '0000-00-00 00:00:00', 1),
(14, 0, 0, 0, '0000-00-00 00:00:00', 1),
(12, 37.5, 98, 72, '0000-00-00 00:00:00', 1),
(12, 0, 0, 0, '2016-07-17 04:37:11', 1),
(12, 37.9, 88, 72, '2016-07-17 04:37:37', 1),
(12, 37.9, 88, 72, '2016-07-17 04:37:41', 1),
(12, 37.9, 88, 72, '2016-07-17 04:37:43', 1),
(12, 37.9, 88, 72, '2016-07-17 04:37:45', 1),
(12, 37.9, 88, 72, '2016-07-17 04:38:05', 1),
(12, 37.9, 88, 72, '2016-07-17 04:41:42', 1),
(12, 37.9, 88, 72, '2016-07-17 04:43:32', 1),
(12, 37.9, 87, 73, '2016-07-17 04:45:06', 1),
(12, 37.9, 87, 73, '2016-07-17 04:46:19', 1),
(14, 37.9, 87, 73, '2016-07-17 04:46:39', 1),
(14, 37.9, 87, 73, '2016-07-17 04:46:40', 1),
(14, 37.9, 87, 73, '2016-07-17 04:46:41', 1),
(14, 37.9, 87, 73, '2016-07-17 04:46:42', 1),
(14, 37.9, 87, 73, '2016-07-17 04:46:42', 1),
(14, 37.9, 87, 73, '2016-07-17 04:46:43', 1),
(14, 37.9, 87, 73, '2016-07-17 04:46:44', 1),
(14, 37.9, 87, 73, '2016-07-17 04:46:45', 1),
(14, 37.9, 87, 73, '2016-07-17 05:02:59', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:27', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:43', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:45', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:47', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:49', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:51', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:53', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:55', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:57', 1),
(14, 37.9, 87, 73, '2016-07-17 05:03:59', 1),
(14, 37.9, 87, 73, '2016-07-17 05:04:00', 1),
(14, 37.9, 87, 73, '2016-07-17 05:04:01', 1),
(14, 37.9, 87, 73, '2016-07-17 05:04:02', 1),
(14, 37.9, 87, 73, '2016-07-17 05:04:03', 1),
(14, 37.9, 87, 73, '2016-07-17 05:04:04', 1),
(14, 37.9, 87, 73, '2016-07-17 05:24:29', 1),
(14, 37.9, 87, 73, '2016-07-17 05:36:37', 1),
(14, 37.9, 87, 73, '2016-07-17 05:52:11', 1),
(14, 37.9, 87, 73, '2016-07-17 05:52:13', 1),
(14, 37.9, 87, 73, '2016-07-17 05:52:14', 1),
(14, 37.9, 87, 73, '2016-07-17 05:57:22', 1),
(14, 37.9, 87, 73, '2016-07-17 05:57:25', 1),
(14, 37.9, 87, 73, '2016-07-17 06:02:32', 1),
(14, 37.9, 87, 73, '2016-07-17 06:10:04', 1),
(16, 37, 88, 73, '2016-07-19 04:34:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `device_id` int(100) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(100) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `device_log`
--

CREATE TABLE IF NOT EXISTS `device_log` (
  `device_id` int(100) NOT NULL,
  `patient_id` int(100) NOT NULL,
  `datetime` datetime NOT NULL,
  `activity` int(2) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE IF NOT EXISTS `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, '123456ouaoidkjuidsxiudinjdsjnds', 0, 0, 0, NULL, 0),
(2, 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', 0, 0, 0, NULL, 0),
(3, 'Q4Ja6isRqtlJHPgYaU8hUyvs0yLqHgfo', 0, 0, 0, NULL, 0),
(4, 'zDKdnjnrYWi8xSXI4frLa7bFzVK61Xdc', 0, 0, 0, NULL, 0),
(5, '8ctxQjvTm8J8zjTJZY0XKAlkoFGtQBJt', 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=886 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `uri`, `method`, `params`, `api_key`, `ip_address`, `time`, `rtime`, `authorized`, `response_code`) VALUES
(425, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160716, 0.301018, '1', 200),
(426, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160759, 0.17401, '1', 200),
(427, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160788, 0.191011, '1', 200),
(428, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160813, 0.163009, '1', 200),
(429, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160814, 0.144008, '1', 200),
(430, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160870, 0.168009, '1', 200),
(431, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160873, 0.161009, '1', 200),
(432, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160874, 0.145008, '1', 200),
(433, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160875, 0.16901, '1', 200),
(434, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160876, 0.156008, '1', 200),
(435, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160933, 0.168009, '1', 200),
(436, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160976, 0.217013, '1', 200),
(437, 'login', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468160989, 0.233013, '1', 200),
(438, 'data', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163419, 0.0810049, '1', 0),
(439, 'data', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163564, 0.105006, '1', 0),
(440, 'data', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163566, 0.085005, '1', 0),
(441, 'data', 'post', 'a:2:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163605, 0.193011, '1', 200),
(442, 'data', 'post', 'a:3:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163616, 0.210012, '1', 200),
(443, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163645, 0.270015, '1', 200),
(444, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163673, 0.214012, '1', 200),
(445, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163859, 0.224013, '1', 200),
(446, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:2:"37";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163864, 0.201011, '1', 200),
(447, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163871, 0.260015, '1', 200),
(448, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163890, 0.152008, '1', 200),
(449, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:2:"37";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163895, 0.218012, '1', 200),
(450, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:3:"mie";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163900, 0.151009, '1', 200),
(451, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163961, 0.212012, '1', 200),
(452, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:5:"1212s";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163966, 0.145009, '1', 200),
(453, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:25:"1212.12918279038928788912";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163975, 0.120007, '1', 200),
(454, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:25:"1212.12918279038928788912";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468163999, 0.135008, '1', 200),
(455, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164009, 0.135008, '1', 200),
(456, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164226, 0.302018, '1', 200),
(457, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164251, 0.52503, '1', 200),
(458, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164253, 0.803046, '1', 200),
(459, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164413, 0.485028, '1', 200),
(460, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164415, 0.420024, '1', 200),
(461, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164416, 0.308017, '1', 200),
(462, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164416, 0.240014, '1', 200),
(463, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164417, 0.52003, '1', 200),
(464, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164418, 0.264016, '1', 200),
(465, 'data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164468, 0.284016, '1', 200),
(466, 'patient_data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164475, 0.114007, '1', 0),
(467, 'patient_data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164569, 0.0960059, '1', 0),
(468, 'patient_data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468164593, 0.214013, '1', 200),
(469, 'patient_data', 'post', 'a:6:{s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165205, 0.141008, '1', 0),
(470, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165223, 0.103006, '1', 0),
(471, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165310, 0.235014, '1', 200),
(472, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165312, 0.234013, '1', 200),
(473, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165328, 0.0630028, '1', 0),
(474, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165344, 0.197012, '1', 200),
(475, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:2:"29";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165610, 0.202011, '1', 200),
(476, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:4:"2133";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165618, 0.144008, '1', 200),
(477, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:4:"2133";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468165629, 0.233014, '1', 200),
(478, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:4:"2133";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166427, NULL, '1', 0),
(479, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:4:"2133";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166451, 0.402023, '1', 200),
(480, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166507, 0.305017, '1', 200),
(481, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166512, 0.0710042, '1', 0),
(482, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166569, 0.255015, '1', 200),
(483, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166572, 0.189011, '1', 200),
(484, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166613, 0.228013, '1', 200),
(485, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166642, 0.223013, '1', 200),
(486, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166756, 0.232013, '1', 200),
(487, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:2:"12";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166766, 0.242014, '1', 200),
(488, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166771, 0.236013, '1', 200),
(489, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166875, 0.238014, '1', 200),
(490, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:2:"11";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468166881, 0.263015, '1', 400),
(491, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:2:"11";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167418, 0.261015, '1', 400),
(492, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167422, 0.251015, '1', 200),
(493, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"12";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167446, 0.282016, '1', 200),
(494, 'data', 'post', 'a:5:{s:2:"id";s:2:"13";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167464, 0.248014, '1', 200),
(495, 'data', 'post', 'a:5:{s:2:"id";s:2:"13";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167465, 0.427024, '1', 200),
(496, 'data', 'post', 'a:5:{s:2:"id";s:2:"13";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167466, 0.307017, '1', 200),
(497, 'data', 'post', 'a:5:{s:2:"id";s:2:"13";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167466, 0.420024, '1', 200),
(498, 'data', 'post', 'a:5:{s:2:"id";s:2:"13";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167467, 0.288017, '1', 200),
(499, 'data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167473, 0.264015, '1', 200),
(500, 'data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167474, 0.258014, '1', 200),
(501, 'data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167474, 0.297017, '1', 200),
(502, 'data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167475, 0.604035, '1', 200),
(503, 'data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167476, 0.473027, '1', 200),
(504, 'data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167477, 0.316018, '1', 200),
(505, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167485, 0.266015, '1', 200),
(506, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167491, 0.16801, '1', 200),
(507, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167508, 0.248014, '1', 200),
(508, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167712, 0.16101, '1', 200),
(509, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167912, 0.17501, '1', 200),
(510, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167962, 0.242014, '1', 200),
(511, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468167985, 0.257015, '1', 200),
(512, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168192, 0.086005, '1', 0),
(513, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168222, 0.152009, '1', 0),
(514, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168245, 0.191011, '1', 200),
(515, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168393, 0.240014, '1', 200),
(516, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168409, 0.203012, '1', 200),
(517, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168415, 0.16001, '1', 200),
(518, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168425, 0.274015, '1', 200),
(519, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168433, 0.235013, '1', 200),
(520, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168519, 0.224013, '1', 200),
(521, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168537, 0.217013, '1', 200),
(522, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168568, 0.196011, '1', 200),
(523, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168580, 0.297017, '1', 200),
(524, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168796, 0.201011, '1', 200),
(525, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168821, 0.253014, '1', 200),
(526, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168834, 0.266015, '1', 200),
(527, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168869, 0.224013, '1', 200),
(528, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168876, 0.239014, '1', 200),
(529, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168877, 0.212012, '1', 200),
(530, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168877, 0.278016, '1', 200),
(531, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468168878, 0.396022, '1', 200),
(532, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169083, 2.55415, '1', 200),
(533, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169095, 1.03506, '1', 200),
(534, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169101, 0.837047, '1', 200),
(535, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169103, 0.833047, '1', 200),
(536, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169105, 1.30708, '1', 200),
(537, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169107, 0.919053, '1', 200),
(538, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169108, 0.926053, '1', 200),
(539, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169109, 0.767044, '1', 200),
(540, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169111, 0.902051, '1', 200),
(541, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169113, 0.780045, '1', 200),
(542, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169114, 0.916052, '1', 200),
(543, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169116, 0.904051, '1', 200),
(544, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169121, 1.05806, '1', 200),
(545, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169122, 1.01906, '1', 200),
(546, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169124, 0.969055, '1', 200),
(547, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169126, 1.10106, '1', 200),
(548, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169127, 0.843048, '1', 200),
(549, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169129, 0.841048, '1', 200),
(550, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169131, 1.34908, '1', 200),
(551, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169137, 0.87705, '1', 200),
(552, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169139, 1.31107, '1', 200),
(553, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169143, 0.88005, '1', 200),
(554, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169145, 0.775044, '1', 200),
(555, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169221, 0.291016, '1', 200),
(556, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169230, 0.221013, '1', 200),
(557, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169241, 0.133008, '1', 200),
(558, 'patient_data', 'post', 'a:5:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169522, 0.158009, '1', 200),
(559, 'patient_data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:1:"1";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169531, 0.192011, '1', 200),
(560, 'patient_data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468169539, 0.222012, '1', 200),
(561, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170300, 1.37608, '1', 200),
(562, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170307, 0.805046, '1', 200),
(563, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170518, 1.48408, '1', 200),
(564, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170522, 0.910052, '1', 200),
(565, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170524, 0.827048, '1', 200),
(566, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170525, 0.987057, '1', 200),
(567, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170527, 0.87405, '1', 200),
(568, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170528, 1.22707, '1', 200),
(569, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170641, 1.10206, '1', 200),
(570, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170647, 0.859049, '1', 200),
(571, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468170702, 0.965055, '1', 200),
(572, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468171120, 1.15907, '1', 200),
(573, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468209747, 1.46108, '1', 200),
(574, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468209861, 0.656037, '1', 200),
(575, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468209863, 0.532031, '1', 200),
(576, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468209865, 0.226013, '1', 200),
(577, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468209866, 0.198011, '1', 200),
(578, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468209867, 0.277016, '1', 200),
(579, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468209914, 1.13306, '1', 200),
(580, 'data', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468209917, 0.88105, '1', 200),
(581, 'login', 'post', 'a:6:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468224419, 0.970056, '1', 400),
(582, 'login', 'post', 'a:8:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468224439, 2.73116, '1', 200),
(583, 'login', 'post', 'a:8:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468224582, 1.33708, '1', 200),
(584, 'patient_data', 'post', 'a:8:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468226220, 0.226013, '1', 200),
(585, 'register', 'post', 'a:8:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468226894, 0.327019, '1', 400),
(586, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0706677565";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468226915, 0.341019, '1', 400),
(587, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0706677565";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468227059, 0.228013, '1', 400),
(588, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0706677565";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468227061, 0.315018, '1', 400),
(589, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:23:"betaclifford@gmail.com1";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0706677565";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468227183, 0.53403, '1', 0),
(590, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:23:"betaclifford@gmail.com1";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0706677565";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468227226, 0.181011, '1', 0),
(591, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:23:"betaclifford@gmail.com1";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0706677565";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468227307, 0.34402, '1', 400),
(592, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:23:"betaclifford@gmail.com2";s:8:"password";s:5:"12345";s:5:"phone";s:12:"070667756521";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468227321, 0.310018, '1', 200),
(593, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:23:"betaclifford@gmail.com2";s:8:"password";s:5:"12345";s:5:"phone";s:12:"070667756521";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468227326, 0.418023, '1', 400),
(594, 'register', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.bom";s:8:"password";s:5:"12345";s:5:"phone";s:11:"07066775652";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468227396, 1.6931, '1', 200),
(595, 'login', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.bom";s:8:"password";s:5:"12345";s:5:"phone";s:11:"07066775652";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468234276, 1.88811, '1', 200),
(596, '', 'get', NULL, '', '::1', 1468235328, 0.254015, '0', 403),
(597, 'patient', 'get', NULL, '', '::1', 1468235371, 0.385022, '0', 403),
(598, 'patient', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.bom";s:8:"password";s:5:"12345";s:5:"phone";s:11:"07066775652";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235384, 0.69804, '1', 200),
(599, 'patient', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.bom";s:8:"password";s:5:"12345";s:5:"phone";s:11:"07066775652";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235391, 0.532031, '1', 0),
(600, 'patient', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.bom";s:8:"password";s:5:"12345";s:5:"phone";s:11:"07066775652";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235434, 0.767044, '1', 0),
(601, 'patient', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.bom";s:8:"password";s:5:"12345";s:5:"phone";s:11:"07066775652";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235599, 0.364021, '1', 200),
(602, 'patient', 'post', 'a:10:{s:2:"id";s:2:"14";s:1:"T";s:4:"37.6";s:1:"P";s:2:"81";s:1:"H";s:2:"71";s:7:"patient";s:1:"3";s:5:"limit";s:2:"10";s:5:"email";s:22:"betaclifford@gmail.bom";s:8:"password";s:5:"12345";s:5:"phone";s:11:"07066775652";s:4:"name";s:4:"Clif";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235610, 0.473027, '1', 200),
(603, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235706, 0.649037, '1', 200),
(604, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235767, 0.684039, '1', 200),
(605, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235794, 0.501029, '1', 200),
(606, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235796, 0.445026, '1', 200),
(607, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235797, 0.291017, '1', 200),
(608, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235798, 0.446025, '1', 200),
(609, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235799, 0.368021, '1', 200),
(610, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235800, 0.234013, '1', 200),
(611, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235800, 0.406024, '1', 200),
(612, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235802, 0.267015, '1', 200),
(613, 'patient', 'post', 'a:1:{s:4:"name";s:4:"Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235803, 0.294017, '1', 200),
(614, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"0";s:4:"ward";s:1:"0";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235833, 0.18501, '1', 0),
(615, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"0";s:4:"ward";s:1:"0";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235861, 0.425025, '1', 200),
(616, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"1";s:4:"ward";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235923, 0.255015, '1', 200),
(617, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"1";s:4:"ward";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235926, 0.233013, '1', 200),
(618, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"1";s:4:"ward";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235927, 0.194011, '1', 200),
(619, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"1";s:4:"ward";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235928, 0.193011, '1', 200),
(620, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"1";s:4:"ward";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235928, 0.334019, '1', 200),
(621, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"1";s:4:"ward";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235929, 0.253014, '1', 200),
(622, 'patient', 'post', 'a:3:{s:4:"name";s:4:"Beta";s:3:"bed";s:1:"1";s:4:"ward";s:1:"2";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235930, 0.178011, '1', 200),
(623, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"1";s:4:"ward";s:1:"3";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235977, 0.304017, '1', 200),
(624, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"3";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235983, 0.429025, '1', 200),
(625, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235990, 0.312018, '1', 200),
(626, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468235994, 0.128007, '1', 200),
(627, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236056, 0.34002, '1', 200),
(628, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236058, 0.18201, '1', 200),
(629, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236059, 0.562032, '1', 200),
(630, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236308, 0.833048, '1', 200),
(631, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236314, 0.516029, '1', 200),
(632, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236329, 0.329019, '1', 200),
(633, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236331, 0.297017, '1', 200),
(634, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236471, 0.376021, '1', 200),
(635, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236499, 0.792045, '1', 200),
(636, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236534, 0.635036, '1', 200),
(637, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236566, 0.86805, '1', 200),
(638, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236602, 0.562032, '1', 200),
(639, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236738, 0.755043, '1', 200),
(640, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236749, 0.418024, '1', 200),
(641, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236764, 0.445026, '1', 200),
(642, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236766, 0.34702, '1', 200),
(643, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236767, 0.487027, '1', 200),
(644, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236768, 0.365021, '1', 200),
(645, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236769, 0.452026, '1', 200),
(646, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:1:"9";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236771, 0.328018, '1', 200),
(647, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236785, 0.501029, '1', 200),
(648, 'patient', 'post', 'a:3:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236882, 0.610035, '1', 400),
(649, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236894, 0.508029, '1', 200),
(650, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236898, 0.252015, '1', 200),
(651, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236901, 0.268015, '1', 200),
(652, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236904, 0.354021, '1', 200),
(653, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236905, 0.633036, '1', 200),
(654, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236908, 0.448025, '1', 200),
(655, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236952, 0.417024, '1', 200),
(656, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236953, 0.610035, '1', 200),
(657, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236955, 0.499029, '1', 200),
(658, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236956, 0.374022, '1', 200),
(659, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236957, 0.636037, '1', 200),
(660, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236958, 0.561032, '1', 200),
(661, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236959, 0.681039, '1', 200),
(662, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236961, 0.424024, '1', 200),
(663, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236962, 0.510029, '1', 200),
(664, 'patient', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468236963, 0.442025, '1', 200),
(665, 'patient_actions', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237702, 0.333019, '1', 500),
(666, 'patient_actions', 'post', 'a:4:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237756, 0.318018, '1', 400),
(667, 'patient_actions', 'post', 'a:5:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"0";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237795, 0.425024, '1', 400),
(668, 'patient_actions', 'post', 'a:6:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"0";s:2:"id";s:2:"12";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237802, 1.06606, '1', 500);
INSERT INTO `logs` (`id`, `uri`, `method`, `params`, `api_key`, `ip_address`, `time`, `rtime`, `authorized`, `response_code`) VALUES
(669, 'patient_actions', 'post', 'a:6:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"12";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237825, 0.774045, '1', 500),
(670, 'patient_actions', 'post', 'a:6:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"0";s:2:"id";s:2:"14";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237845, 0.449025, '1', 500),
(671, 'patient_actions', 'post', 'a:6:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237852, 0.475027, '1', 200),
(672, 'patient_actions', 'post', 'a:6:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"0";s:2:"id";s:2:"14";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237872, 0.603035, '1', 200),
(673, 'patient_actions', 'post', 'a:6:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"0";s:2:"id";s:2:"14";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237880, 0.463026, '1', 500),
(674, 'patient_actions', 'post', 'a:6:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237887, 0.502029, '1', 200),
(675, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"12";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468237913, 0.432025, '1', 500),
(676, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"12";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468238037, 0.505029, '1', 400),
(677, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468238044, 0.69904, '1', 500),
(678, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468238101, 0.282016, '1', 400),
(679, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468238119, 0.397023, '1', 200),
(680, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468238121, 0.306017, '1', 400),
(681, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468238124, 0.335019, '1', 400),
(682, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468238126, 0.313018, '1', 400),
(683, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '::1', 1468238161, 0.391022, '1', 400),
(684, 'patient', 'get', NULL, '', '::1', 1468238459, 0.242014, '0', 403),
(685, '', 'get', NULL, '', '197.232.244.52', 1468341980, 0.00258112, '0', 403),
(686, 'login', 'get', NULL, '', '197.232.244.52', 1468341985, 0.00221109, '0', 403),
(687, 'patient', 'get', NULL, '', '197.232.244.52', 1468341991, 0.00282311, '0', 403),
(688, 'patient_actions', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468342016, 0.00765705, '1', 400),
(689, 'patient', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468342026, 0.00431013, '1', 200),
(690, 'patient_data', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468342035, 0.00518513, '1', 200),
(691, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:22:"b.clifford@sendy.co.ke";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '107.22.26.74', 1468342203, 0.00454903, '1', 400),
(692, 'login', 'post', 'a:7:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468342220, 0.006387, '1', 400),
(693, 'login', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468342243, 0.171777, '1', 200),
(694, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '54.227.156.137', 1468342310, 0.189217, '1', 200),
(695, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '107.22.26.74', 1468349753, 0.177941, '1', 200),
(696, 'login', 'get', 'a:3:{s:9:"x-api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '', '197.179.89.64', 1468349800, 0.00268197, '0', 403),
(697, 'login', 'get', 'a:3:{s:9:"x-api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '', '197.179.89.64', 1468349819, 0.0026269, '0', 403),
(698, 'login', 'get', 'a:2:{s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '', '197.179.89.64', 1468349850, 0.00220609, '0', 403),
(699, 'login', 'post', NULL, '', '197.179.89.64', 1468359652, 0.00240707, '0', 403),
(700, 'login', 'post', NULL, '', '197.179.89.64', 1468359660, 0.00268483, '0', 403),
(701, 'login', 'post', NULL, '', '197.179.89.64', 1468359719, 0.0024631, '0', 403),
(702, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468360678, 0.186751, '1', 200),
(703, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468360781, 0.200537, '1', 200),
(704, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"betaclifford@gmail2.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468360816, 0.00379705, '1', 400),
(705, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468361007, 0.187237, '1', 200),
(706, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468361303, 0.185816, '1', 200),
(707, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:21:"betaclifford@gmail.co";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468361473, 0.0502701, '1', 400),
(708, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468361478, 0.171421, '1', 200),
(709, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"cliffordbeta@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468361600, 0.00416207, '1', 400),
(710, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468361620, 0.175582, '1', 200),
(711, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468361703, 0.171318, '1', 200),
(712, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.179.89.64', 1468362766, 0.286906, '1', 200),
(713, 'data', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468409768, 0.397542, '1', 200),
(714, 'data', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:2:"16";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468409788, 0.185205, '1', 200),
(715, 'patient_actions', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:4:"1122";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411283, 0.180295, '1', 200),
(716, 'patient_actions', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:4:"1122";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411287, 0.00392294, '1', 400),
(717, 'data', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:4:"1122";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411295, 0.170771, '1', 200),
(718, 'data', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:4:"1122";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411298, 0.172485, '1', 200),
(719, 'data', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:4:"1122";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411299, 0.185222, '1', 200),
(720, 'data', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:4:"1122";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411300, 0.199176, '1', 200),
(721, 'data', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:4:"1122";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411301, 0.18709, '1', 200),
(722, 'patient', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:1:"4";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:4:"1122";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411687, 0.00398993, '1', 200),
(723, 'patient', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:2:"43";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:3:"112";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468411710, 0.00867701, '1', 200),
(724, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:22:"b.clifford@sendy.co.ke";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0706677565";s:4:"name";s:13:"Clifford Beta";}', '123456ouaoidkjuidsxiudinjdsjnds', '54.162.204.5', 1468412402, 0.00496197, '1', 400),
(725, 'patient', 'post', 'a:6:{s:7:"api=key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:4:"name";s:13:"Clifford Beta";s:3:"bed";i:37;s:4:"ward";i:72;s:6:"device";i:98;s:4:"data";s:10:"This is it";}', '', '107.22.65.229', 1468412418, 0.00234795, '0', 403),
(726, 'patient', 'post', 'a:6:{s:7:"api=key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:4:"name";s:13:"Clifford Beta";s:3:"bed";i:37;s:4:"ward";i:72;s:6:"device";i:98;s:4:"data";s:10:"This is it";}', '', '54.198.226.43', 1468412447, 0.00252104, '0', 403),
(727, 'patient', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:2:"43";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:3:"112";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468412460, 0.00401497, '1', 200),
(728, 'patient_actions', 'post', NULL, '', '54.162.204.5', 1468412492, 0.00949502, '0', 403),
(729, 'patient_actions', 'post', NULL, '', '54.91.124.66', 1468412590, 0.00276613, '0', 403),
(730, 'patient_actions', 'post', NULL, '', '54.81.44.11', 1468412609, 0.00234818, '0', 403),
(731, 'data', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:2:"id";i:12;s:1:"T";d:37.5;s:1:"H";i:72;s:1:"P";i:98;}', '123456ouaoidkjuidsxiudinjdsjnds', '54.91.124.66', 1468412652, 0.193591, '1', 200),
(732, 'patient_data', 'post', 'a:2:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:7:"patient";i:3;}', '123456ouaoidkjuidsxiudinjdsjnds', '54.227.156.137', 1468412672, 0.0045011, '1', 200),
(733, 'patient', 'post', 'a:6:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:4:"name";s:13:"Clifford Beta";s:3:"bed";i:37;s:4:"ward";i:72;s:6:"device";i:98;s:4:"data";s:10:"This is it";}', '123456ouaoidkjuidsxiudinjdsjnds', '54.81.44.11', 1468412688, 0.189632, '1', 200),
(734, 'patient', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:2:"43";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:3:"112";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468412955, 0.00558305, '1', 200),
(735, 'patient', 'post', 'a:9:{s:4:"name";s:8:"Beta C B";s:3:"bed";s:2:"44";s:4:"ward";s:2:"12";s:7:"command";s:6:"create";s:6:"action";s:1:"1";s:2:"id";s:2:"14";s:6:"device";s:3:"112";s:5:"email";s:22:"betaclifford@gmail.com";s:8:"password";s:5:"12345";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.232.244.52', 1468412970, 0.169696, '1', 200),
(736, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468490741, 0.366566, '1', 200),
(737, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468490906, 0.170859, '1', 200),
(738, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468490961, 0.169176, '1', 200),
(739, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468491425, 0.184379, '1', 200),
(740, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468491549, 0.185929, '1', 200),
(741, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468497820, 0.17431, '1', 200),
(742, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468498439, 0.222526, '1', 200),
(743, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:23:"dennokareithi@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:11:"07150722135";s:4:"name";s:14:"Dennis Muriuki";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468501983, 0.170623, '1', 200),
(744, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468502005, 0.171655, '1', 200),
(745, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:25:"dennokareithi@outlook.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:14:"Muriuki Dennis";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468503794, 0.237778, '1', 200),
(746, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:25:"dennokareithi@outlook.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468503817, 0.18622, '1', 200),
(747, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:15:"denno@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:9:"715072213";s:4:"name";s:14:"Denno Kareithi";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468504870, 0.186683, '1', 200),
(748, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:14:"adar@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0701411321";s:4:"name";s:9:"Adar Zeph";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468505340, 0.17608, '1', 200),
(749, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:18:"onekevin@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:9:"Kevin One";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468505653, 0.00455785, '1', 400),
(750, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:18:"onekevin@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:9:"Kevin One";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468505665, 0.00418806, '1', 400),
(751, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:18:"onekevin@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:9:"Kevin One";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468505722, 0.00411606, '1', 400),
(752, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:18:"onekevin@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:9:"Kevin One";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468505729, 0.0047791, '1', 400),
(753, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:18:"onekevin@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0774827213";s:4:"name";s:9:"Kevin One";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468505774, 0.207933, '1', 200),
(754, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:18:"onekevin@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468505781, 0.172643, '1', 200),
(755, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:13:"123@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468507275, 0.00550699, '1', 400),
(756, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:23:"dennokareithi@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:4:"Deno";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468507319, 0.00422192, '1', 400),
(757, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:13:"123@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468507370, 0.00457907, '1', 400),
(758, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:23:"dennokareithi@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:6:"Dennis";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468507390, 0.0036931, '1', 400),
(759, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:13:"123@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468507742, 0.00371194, '1', 400),
(760, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:23:"dennokareithi@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:6:"Dennis";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468507790, 0.0038569, '1', 400),
(761, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:13:"123@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468508029, 0.00409198, '1', 400),
(762, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:13:"123@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468508118, 0.00328898, '1', 400),
(763, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:13:"123@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468508309, 0.00426793, '1', 400),
(764, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:13:"123@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '212.49.88.106', 1468508818, 0.00393891, '1', 400),
(765, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468564562, 0.427978, '1', 200),
(766, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468564847, 0.170366, '1', 200),
(767, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468564859, 0.17164, '1', 200),
(768, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.cok";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468574315, 0.00430703, '1', 400),
(769, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.cok";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468574346, 0.00448394, '1', 400),
(770, 'register', 'post', 'a:5:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:5:"email";s:23:"dennokareithi@gmail.com";s:8:"password";s:5:"12345";s:5:"phone";s:10:"0715072213";s:4:"name";s:6:"Dennis";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468574395, 0.0372832, '1', 400),
(771, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468574499, 0.171862, '1', 200),
(772, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468574779, 0.203063, '1', 200),
(773, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468574783, 0.187153, '1', 200),
(774, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468574843, 0.198488, '1', 200),
(775, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468576579, 0.17986, '1', 200),
(776, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468583817, 0.186694, '1', 200),
(777, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468584484, 0.170973, '1', 200),
(778, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468584668, 0.419355, '1', 200),
(779, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468586030, 0.186593, '1', 200),
(780, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468586462, 0.171037, '1', 200),
(781, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1468586492, 0.170369, '1', 200),
(782, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.49.8.8', 1468600091, 0.828289, '1', 200),
(783, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.49.8.8', 1468602017, 0.172864, '1', 200),
(784, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.49.8.8', 1468602242, 0.18722, '1', 200),
(785, 'patient_data', 'post', 'a:2:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:7:"patient";i:3;}', '123456ouaoidkjuidsxiudinjdsjnds', '54.226.160.155', 1468662838, 0.341955, '1', 200),
(786, 'patient_data', 'post', 'a:2:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:7:"patient";i:3;}', '123456ouaoidkjuidsxiudinjdsjnds', '23.23.53.202', 1468662867, 0.0614631, '1', 200),
(787, 'patient_data', 'post', 'a:2:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:7:"patient";i:3;}', '123456ouaoidkjuidsxiudinjdsjnds', '54.226.160.155', 1468662929, 0.106087, '1', 200),
(788, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"betaclifford@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468663812, 0.232949, '1', 200),
(789, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:18:"adarzeph@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468665476, 0.090662, '1', 400),
(790, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:14:"adar@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468665481, 0.275005, '1', 200),
(791, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468665569, 0.208736, '1', 200),
(792, 'patient_data', 'post', 'a:2:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:7:"patient";s:1:"3";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468670203, 0.00488305, '1', 200),
(793, 'patient_data', 'post', 'a:1:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468670294, 0.00497293, '1', 200),
(794, 'patient_data', 'post', 'a:1:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468670531, 0.00353098, '1', 200),
(795, 'patient_data', 'post', NULL, '', '105.58.28.129', 1468671233, 0.00307703, '0', 403),
(796, 'patient_data', 'post', NULL, '', '105.58.28.129', 1468671255, 0.00271511, '0', 403),
(797, 'patient_data', 'post', 'a:1:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468671277, 0.00474906, '1', 200),
(798, 'patient_data', 'post', 'a:1:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";}', '123456ouaoidkjuidsxiudinjdsjnds', '105.58.28.129', 1468671649, 0.00364184, '1', 200),
(799, '', 'get', NULL, '', '197.180.12.2', 1468762476, 0.0833349, '0', 403),
(800, '', 'post', NULL, '', '197.180.12.2', 1468762484, 0.00282311, '0', 403),
(801, 'patient_data', 'post', NULL, '', '197.180.12.2', 1468762495, 0.00271606, '0', 403),
(802, 'patient_data', 'post', NULL, '', '197.180.12.2', 1468762507, 0.00216794, '0', 403),
(803, 'patient_data', 'post', NULL, 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762572, 0.0187359, '1', 200),
(804, 'patient_data', 'post', 'a:1:{s:1:"H";s:0:"";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762583, 0.0277231, '1', 200),
(805, 'patient_data', 'post', NULL, 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762596, 0.00405908, '1', 200),
(806, 'data', 'post', NULL, 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762610, 0.00315189, '1', 400),
(807, 'data', 'post', 'a:1:{s:1:"H";s:0:"";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762616, 0.00272202, '1', 400),
(808, 'data', 'post', 'a:1:{s:2:"id";s:2:"12";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762631, 0.179241, '1', 200),
(809, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"72";s:1:"T";s:4:"37.9";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762657, 0.208049, '1', 200),
(810, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"72";s:1:"T";s:4:"37.9";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762661, 0.20513, '1', 200),
(811, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"72";s:1:"T";s:4:"37.9";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762663, 0.19965, '1', 200),
(812, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"72";s:1:"T";s:4:"37.9";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762665, 0.201936, '1', 200),
(813, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"72";s:1:"T";s:4:"37.9";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762685, 0.185323, '1', 200),
(814, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"72";s:1:"T";s:4:"37.9";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468762902, 0.172928, '1', 200),
(815, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"72";s:1:"T";s:4:"37.9";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763012, 0.171734, '1', 200),
(816, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763106, 0.185864, '1', 200),
(817, 'data', 'post', 'a:4:{s:2:"id";s:2:"12";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763179, 0.175922, '1', 200),
(818, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763199, 0.186523, '1', 200),
(819, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763200, 0.199713, '1', 200),
(820, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763201, 0.171545, '1', 200),
(821, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763202, 0.170527, '1', 200),
(822, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763202, 0.184133, '1', 200),
(823, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763203, 0.170744, '1', 200),
(824, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763204, 0.185587, '1', 200),
(825, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.180.12.2', 1468763205, 0.169596, '1', 200),
(826, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764179, 0.193111, '1', 200),
(827, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764207, 0.185034, '1', 200),
(828, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764223, 0.186359, '1', 200),
(829, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764225, 0.18505, '1', 200),
(830, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764227, 0.170768, '1', 200),
(831, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764229, 0.185155, '1', 200),
(832, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764231, 0.185653, '1', 200),
(833, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764233, 0.170341, '1', 200),
(834, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764235, 0.171939, '1', 200),
(835, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764237, 0.171204, '1', 200),
(836, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764239, 0.185781, '1', 200),
(837, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764240, 0.186116, '1', 200),
(838, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764241, 0.185963, '1', 200),
(839, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764242, 0.186594, '1', 200),
(840, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764243, 0.200024, '1', 200),
(841, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '41.80.252.89', 1468764244, 0.18503, '1', 200),
(842, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.176.149.53', 1468764945, 0.171649, '1', 200),
(843, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.176.149.53', 1468765146, 0.169739, '1', 200),
(844, 'data', 'get', NULL, '', '105.58.223.138', 1468765149, 0.00261307, '0', 403),
(845, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '197.176.149.53', 1468765427, 0.00297713, '1', 200),
(846, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '197.176.149.53', 1468765454, 0.00294209, '1', 200),
(847, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468765469, 0.199433, '1', 200),
(848, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468766197, 0.184616, '1', 200),
(849, 'auth', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', '', '105.58.223.138', 1468766206, 0.00258589, '1', 400),
(850, 'data', 'get', NULL, '', '105.58.223.138', 1468766325, 0.00246406, '0', 403),
(851, 'data', 'get', NULL, '', '105.58.223.138', 1468766342, 0.00240111, '0', 403),
(852, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.176.149.53', 1468766396, 0.171457, '1', 200),
(853, 'data', 'get', NULL, '', '105.58.223.138', 1468766479, 0.00261807, '0', 403),
(854, 'data', 'get', NULL, '', '105.58.223.138', 1468766526, 0.00212908, '0', 403),
(855, 'data', 'get', NULL, '', '105.58.223.138', 1468766568, 0.00237894, '0', 403),
(856, 'data', 'get', NULL, '', '105.58.223.138', 1468766684, 0.00301003, '0', 403),
(857, 'data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '105.58.223.138', 1468766841, 0.00445604, '1', 400),
(858, 'data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '105.58.223.138', 1468766918, 0.00294709, '1', 400),
(859, 'data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '105.58.223.138', 1468767065, 0.00350809, '1', 400),
(860, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '105.58.223.138', 1468767080, 0.00285316, '1', 200),
(861, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '105.58.223.138', 1468767091, 0.00330186, '1', 200),
(862, 'auth', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', '', '105.58.223.138', 1468767116, 0.00266099, '1', 400),
(863, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468767131, 0.170703, '1', 200),
(864, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468767133, 0.187205, '1', 200),
(865, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468767134, 0.202971, '1', 200),
(866, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468767442, 0.186213, '1', 200),
(867, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468767445, 0.172047, '1', 200),
(868, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468767752, 0.198425, '1', 200),
(869, 'data', 'post', 'a:4:{s:2:"id";s:2:"14";s:1:"H";s:2:"73";s:1:"T";s:4:"37.9";s:1:"P";s:2:"87";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '105.58.223.138', 1468768204, 0.239906, '1', 200),
(870, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '197.177.39.105', 1468772701, 0.227711, '1', 200),
(871, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '196.97.30.147', 1468925907, 0.511355, '1', 200),
(872, 'patient_data', 'post', 'a:4:{s:2:"id";s:2:"16";s:1:"H";s:2:"73";s:1:"T";s:2:"37";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.232.244.52', 1468935245, 0.192648, '1', 200),
(873, 'data', 'post', 'a:4:{s:2:"id";s:2:"16";s:1:"H";s:2:"73";s:1:"T";s:2:"37";s:1:"P";s:2:"88";}', 'KA83T9iQm4oK0Jjcnid4X7AQOvxFmwl5', '197.232.244.52', 1468935258, 0.190858, '1', 200),
(874, 'patient_data', 'post', NULL, '', '196.101.157.232', 1468942191, 0.00268507, '0', 403),
(875, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '196.101.157.232', 1468942222, 0.00349212, '1', 200),
(876, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '196.101.157.232', 1468942237, 0.00346994, '1', 200),
(877, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '196.101.157.232', 1468942265, 0.00639796, '1', 200),
(878, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '196.101.157.232', 1468942280, 0.140344, '1', 200),
(879, 'patient_data', 'post', 'a:2:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:7:"patient";i:3;}', '123456ouaoidkjuidsxiudinjdsjnds', '54.80.157.50', 1468942355, 0.00446796, '1', 200),
(880, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '196.101.157.232', 1468942372, 0.0320139, '1', 200),
(881, 'patient_data', 'post', NULL, '123456ouaoidkjuidsxiudinjdsjnds', '196.101.157.232', 1468942405, 0.003407, '1', 200),
(882, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:22:"dennokareithi@gmail.co";}', '123456ouaoidkjuidsxiudinjdsjnds', '196.101.157.232', 1468948167, 0.821231, '1', 400),
(883, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '196.101.157.232', 1468948173, 0.395337, '1', 200),
(884, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1469101240, 0.404479, '1', 200),
(885, 'login', 'post', 'a:3:{s:7:"api-key";s:31:"123456ouaoidkjuidsxiudinjdsjnds";s:8:"password";s:5:"12345";s:5:"email";s:23:"dennokareithi@gmail.com";}', '123456ouaoidkjuidsxiudinjdsjnds', '41.76.168.137', 1469173941, 0.41153, '1', 200);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `member_id` int(100) NOT NULL AUTO_INCREMENT,
  `member_name` text NOT NULL,
  `member_phone` varchar(20) NOT NULL,
  `member_email` varchar(50) NOT NULL,
  `member_gender` text NOT NULL,
  `member_type` int(2) NOT NULL,
  `member_photo` varchar(40) NOT NULL,
  `member_profile` varchar(50) NOT NULL DEFAULT '{"user_type":"normal","department":"null"}',
  `password` varchar(100) NOT NULL,
  `date_signed_up` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `member_name`, `member_phone`, `member_email`, `member_gender`, `member_type`, `member_photo`, `member_profile`, `password`, `date_signed_up`, `status`) VALUES
(1, 'Clifford Beta', '07066775656', 'betaclifford@gmail.com', 'M', 1, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-10 08:12:14', 1),
(2, 'Clif', '0706677565', 'betaclifford@gmail.com1', '', 0, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-11 10:53:47', 1),
(3, 'Clif', '070667756521', 'betaclifford@gmail.com2', '', 0, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-11 10:55:21', 1),
(4, 'Clif', '07066775652', 'betaclifford@gmail.bom', '', 0, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-11 10:56:37', 1),
(5, 'Dennis Muriuki', '07150722135', 'dennokareithi@gmail.com', '', 0, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-14 04:13:03', 1),
(6, 'Muriuki Dennis', '0715072213', 'dennokareithi@outlook.com', '', 0, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-14 04:43:14', 1),
(7, 'Denno Kareithi', '715072213', 'denno@gmail.com', '', 0, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-14 05:01:10', 1),
(8, 'Adar Zeph', '0701411321', 'adar@gmail.com', '', 0, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-14 05:09:00', 1),
(9, 'Kevin One', '0774827213', 'onekevin@gmail.com', '', 0, '', '{"user_type":"normal","department":"null"}', '8cb2237d0679ca88db6464eac60da96345513964', '2016-07-14 05:16:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
  `patient_id` int(100) NOT NULL AUTO_INCREMENT,
  `patient_name` text NOT NULL,
  `device_id` int(100) NOT NULL,
  `patient_data` varchar(200) DEFAULT NULL,
  `bed` int(100) DEFAULT NULL,
  `ward` int(50) DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patient_id`, `patient_name`, `device_id`, `patient_data`, `bed`, `ward`, `datetime`, `status`) VALUES
(1, 'Huyu', 12, 'This is it', 1, 2, '2016-07-10 19:01:40', 1),
(2, 'Yeye', 13, 'This is that', 1, 3, '2016-07-10 19:33:06', 1),
(3, 'Wote', 14, 'Hey', 42, 2, '2016-07-10 19:33:06', 1),
(14, 'Beta C B', 1122, NULL, 4, 12, '2016-07-11 14:09:44', 1),
(15, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:35', 0),
(16, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:36', 0),
(17, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:37', 0),
(18, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:38', 0),
(19, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:39', 0),
(20, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:40', 0),
(21, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:40', 0),
(22, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:42', 1),
(23, 'Beta', 0, NULL, 0, 0, '2016-07-11 14:16:43', 1),
(24, 'Beta C B', 0, NULL, 4, 9, '2016-07-11 14:20:56', 1),
(25, 'Beta C B', 0, NULL, 4, 9, '2016-07-11 14:20:58', 1),
(26, 'Beta C B', 0, NULL, 4, 9, '2016-07-11 14:20:59', 1),
(27, 'Beta C B', 0, NULL, 4, 9, '2016-07-11 14:32:19', 0),
(28, 'Beta C B', 0, NULL, 4, 12, '2016-07-11 14:33:05', 0),
(29, 'Beta C B', 0, NULL, 4, 12, '2016-07-11 14:34:54', 0),
(30, 'Beta C B', 0, NULL, 4, 12, '2016-07-11 14:34:58', 0),
(31, 'Beta C B', 0, NULL, 4, 12, '2016-07-11 14:35:01', 0),
(32, 'Beta C B', 0, NULL, 4, 12, '2016-07-11 14:35:04', 0),
(33, 'Beta C B', 0, NULL, 4, 12, '2016-07-11 14:35:05', 0),
(34, 'Beta C B', 0, NULL, 4, 12, '2016-07-11 14:35:08', 0),
(35, 'Beta C B', 0, NULL, 4, 12, '2016-07-11 14:35:52', 1),
(36, 'Beta C B', 112, NULL, 43, 12, '0000-00-00 00:00:00', 1),
(37, 'Clifford Beta', 98, 'This is it', 37, 72, '0000-00-00 00:00:00', 1),
(38, 'Beta C B', 112, NULL, 44, 12, '2016-07-13 03:29:30', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
