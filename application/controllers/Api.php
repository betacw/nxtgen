<?php defined('BASEPATH') or exit ('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
 header("Access-Control-Allow-Origin: *");
 header("Access-Control-Request-Method: POST");
header("Access-Control-Request-Headers: X-PINGOTHER, Content-Type");
date_default_timezone_set('Africa/Nairobi');

class API extends REST_Controller{

function __contruct(){
     header('Access-Control-Allow-Origin: *');
    // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	parent: __construct();
	$this->load->helper('my_api');

}



//////API FOR NXTGEN PROJECT TECH EXPO 2016

function auth_post(){

//initial auth of the api user to get them an api key, post username and password
$data=$this->post();
// set validation rules
$this->load->library('form_validation');
$this->load->helper('security');
$this->form_validation->set_data($data);
            $this->form_validation->set_rules('phone', 'phone', 'alpha_dash|min_length[9]|max_length[15]|xss_clean');
            $this->form_validation->set_rules('email', 'email', 'required|trim|valid_email|xss_clean');
            $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean' );

             if($this->form_validation->run()!=false ){

                    $this->load->model('maine_model');
                    $login=$this->maine_model->fast_login($data['email'],$data['password']);
                    if($login!=null){
                        // $this->load->model('maine_model');
                        $txt=$login;
                        unset($txt['api_key']);
                        unset($txt['id']);
                        $txt['field']='user';
                $data=array('message'=>json_encode($txt));
                unset($login->api_key);
              $this->maine_model->pusher($data,'auth_channel');
                        $this->response(array('status'=>true,'message'=>$login) );

                    }else{

                        $this->response(array('status'=>false,'message'=>'Unauthorised'),Rest_controller::HTTP_BAD_REQUEST );

                    }
                    // echo json_encode($this->maine_model->fast_login($data['email'],$data['password']));

                }else{

            $this->response(array('status'=>false,'message'=>validation_errors()),Rest_controller:: HTTP_BAD_REQUEST);


                }
}



function data_post(){
//post data to the api and insert in the db
    $data=$this->post();
    $this->form_validation->set_data($data);
    if($this->form_validation->run('data') != FALSE){
        $payload=array(
            'device_id'=>$data['id'],
            'status'=>1,
            'datetime'=>date("Y-m-d h:i:sa"),
            );
                            if (array_key_exists("T",$data))
                             {
                                $payload['temperature']=$data['T'];
                             }
                             if (array_key_exists("P",$data))
                             {
                                $payload['pressure']=$data['P'];
                             }
                             if (array_key_exists("H",$data))
                             {
                                $payload['heartrate']=$data['H'];
                             }
                             if($this->maine_model->insert('data',$payload)){

                              $this->maine_model->firebase($this->maine_model->data_push());

                             $this->response(array('status'=>TRUE, 'message'=>"data inserted"));
                         }else{
                            $this->response(array('status'=>FALSE, 'message'=>"Internal server error"), Rest_controller::HTTP_INTERNAL_SERVER_ERROR);
                         }


    }else{
            $this->response(array('status'=>FALSE, 'message'=>validation_errors()), Rest_controller::HTTP_BAD_REQUEST);

    }



}

function patient_data_post(){
$data=$this->post();
$this->form_validation->set_data($data);

if($this->form_validation->run('filter') != FALSE){


$payload=array('data.status'=>1,);
$limit=NULL;
                            if (array_key_exists("patient",$data))
                             {
                                $payload['patient_id']=$data['patient'];
                             }
                             if (array_key_exists("from",$data))
                             {
                                $payload['data.datetime >=']=date("Y-m-d h:i:sa",strtotime($data['from'].' 00:00:00'));
                             }
                             if (array_key_exists("to",$data))
                             {
                                $payload['data.datetime']=date("Y-m-d h:i:sa",strtotime($data['to'].' 23:59:59'));
                             }
                             if (array_key_exists("limit",$data))
                             {
                                $limit=$data['limit'];
                             }
                             //decide whether to give initial data
if(array_key_exists('patient_id', $payload)){

     $datae=$this->maine_model->get_specific_p('data',$payload,$limit);

}else{
    $datae=$this->maine_model->get_initial_data('data',array('status'=>1));

}


 if($datae!=NULL){
$this->response(array('status' =>TRUE , 'message'=>$datae));
 }else{
  $this->response(array('status' =>FALSE , 'message'=>'No data for patient'), Rest_controller::HTTP_BAD_REQUEST);
 }

}else{

$this->response(array('status' =>FALSE , 'message'=>validation_errors() ));

}



}









function register_post(){

//insert new member

$data=$this->post();
// $this->load->library('form_validation');
    $this->form_validation->set_data($data);
        if($this->form_validation->run('register')!=false ){
            $this->load->model('maine_model');
            $tbl='member';

$payload=array(
$tbl.'_name'=>$data['name'],
$tbl.'_phone'=>$data['phone'],
$tbl.'_email'=>$data['email'],
'password' => sha1($data['password']),
'date_signed_up'=>date("Y-m-d h:i:sa"),
'status'=>1
    );

if($this->maine_model->insert($tbl,$payload)){
    $mm=$this->maine_model->member_push();
    $mm->field='member';
       $data=array('message'=>json_encode($mm));
     $this->maine_model->pusher($data,'patient_channel');
$this->response(array('status'=>TRUE,'message'=>"Member inserted successfully"));
}else{
$this->response(array('status'=>FALSE,'message'=>'Unknown error'), Rest_controller::HTTP_INTERNAL_SERVER_ERROR);

}

        }else{
$this->response(array('status'=>FALSE,'message'=>validation_errors()),Rest_controller::HTTP_BAD_REQUEST);



        }






}

 public function email_check($str)
        {
                if ($this->maine_model->get_specific('member',array('member_email'=>$str)) != null)
                {
                        $this->form_validation->set_message('email_check', 'The {field} field is not unique');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }

 public function phone_check($str)
        {
                if ($this->maine_model->get_specific('member',array('member_phone'=>$str)) != null)
                {
                        $this->form_validation->set_message('phone_check', 'The {field} field is not unique');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }
 public function email_check1($str)
        {
                if ($this->maine_model->get_specific('member',array('member_email'=>$str)) != null)
                {
                    return TRUE;

                }
                else
                {
                    $this->form_validation->set_message('email_check1', 'The {field} field is not registered');
                        return FALSE;

                }
        }



public function login_post(){
$data=$this->post();
if($data != NULL){
    $this->form_validation->set_data($data);
        if($this->form_validation->run('login')!=false ){
            $payload=array(
                'member_email'=>$data['email'],
                'password'=>sha1($data['password'])
                );

            $dat=$this->maine_model->get_specific('member',$payload);
            // $this->response(array('status'=>TRUE,'message'=>$dat));
            if($dat!=null){
             unset($dat[0]->password);

                $data=array('message'=>json_encode($dat));

              $this->maine_model->pusher($data,'auth_channel');

                $this->response(array('status'=>true,'message'=>$dat[0], 'data'=>$datae=$this->maine_model->get_initial_data('data',array('status'=>1,'limit'=>100))));
            }else{
                $this->response(array('status'=>false,'message'=>'unsuccessful login'), Rest_controller::HTTP_BAD_REQUEST);
            }


        }else{
            $this->response(array('status'=>false,'message'=>validation_errors()), Rest_controller::HTTP_BAD_REQUEST);


        }
}else{

   $this->response(array('status'=>false,'message'=>"No data submitted"));
}



}













function patient_post(){
//enter new patient data
    $data=$this->post();
    if(!empty($data)){
    $this->form_validation->set_data($data);
    if($this->form_validation->run('wote')!=false){
        $payload=array('patient_name'=>$data['name'],'status'=>1, 'datetime'=>date("Y-m-d h:i:sa"));
        $tbl='patient';
                        if (array_key_exists("bed",$data)&& array_key_exists("ward", $data))
                             {
                                //check if bed is occuppieds

                            if($this->maine_model->get_specific($tbl,array('bed'=>$data['bed'],'ward'=>$data['ward'],'status'=>1)) != null){
                                    $this->response(array('status'=>false,'message'=>"Bed occupied"));
                            }else{
                              $payload['bed']=$data['bed'];
                                $payload['ward']=$data['ward'];
                            }

                             }

                             if (array_key_exists("device",$data))
                             {
                                $payload['device_id']=$data['device'];
                             }
                             if (array_key_exists("data",$data))
                             {
                                $payload[$tbl.'_data']=$data['data'];
                             }
                             // $this->response(array('status'=>true,'message'=>$payload));
if($this->maine_model->insert($tbl,$payload)){
  $data=array('message'=>json_encode($this->maine_model->patient_push()));
  $this->maine_model->pusher($data,'patient_channel');
    $this->response(array('status'=>true,'message'=>"Patient created"));
}else{
  $this->response(array('status'=>'failure','message'=>'No members'),Rest_controller::HTTP_INTERNAL_SERVER_ERROR);
}
}else{
$this->response(array('status'=>'failure','message'=>validation_errors()),Rest_controller::HTTP_BAD_REQUEST);}
    }

}





function member_actions_get_post(){
        $all=$this->maine_model->get_specific('member_actions',array('status'=>1));
        if($all != NULL){
            $this->response(array('status'=>true,'message'=>$all));
        }else{
            $this->response(array('status'=>'failure','message'=>'No actions configured'),Rest_controller::HTTP_BAD_REQUEST);
        }

}

function patient_actions_post(){
//takes in member type, email and returns success on delete and failure in case operation fails
	$data=$this->post();

	$this->form_validation->set_data($data);
//change name, phone, email,
                if($this->form_validation->run('edit')!=false ){
                    $tb='patient';
                		// var_dump($data['action']);
                    $payload=array();

                        $payload[$tb.'_id']=$data['id'];
switch ($data['action']) {
    case 1:
$payload['status']=1;
if (array_key_exists("name",$data))
{
      $payload[$tb.'_name']=$data['name'];
 }

 if (array_key_exists("bed",$data))
 {
 $payload['bed']=$data['bed'];
 }
if (array_key_exists("ward",$data))
{
$payload['ward']=$data['ward'];
}
if (array_key_exists("device",$data))
{
$payload['device_id']=$data['device'];
}
        break;
    case 0:
$payload['status']=0;
    break;
    default:
        # code...
        break;
}

if($this->maine_model->update($tb,$payload,array('patient_id'=>$payload['patient_id']))){

  $data=array('message'=>json_encode($this->maine_model->get_specific('patient',array('patient_id'=>$payload['patient_id']))));
              $this->maine_model->pusher($data,'patient_channel');
$this->response(array('status'=>true,'message'=>'Patient update successful'));
}else{
$this->response(array('status'=>false,'message'=>'User update unsuccessful'), Rest_controller::HTTP_INTERNAL_SERVER_ERROR);
}
}else{

 $this->response(array('status' =>false ,'message'=>validation_errors()), Rest_controller::HTTP_BAD_REQUEST);

}


}



 public function device_check($str)
        {
                if ($this->maine_model->get_specific('patient',array('device_id'=>$str,'status'=>1)) != null)
                {
                        $this->form_validation->set_message('device_check', 'The {field} is in use');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//payments API
function payments_post(){
    // creates a record of a new payment
$data=$this->post();
$this->form_validation->set_data($data);
                if($this->form_validation->run('payment')!=false ){
$payload=array('member_id' => $data['id'],
					'contribution_id'=>$data['contribution'],
					'contribution_amt'=>$data['amount'],
						'code'=>"jkctr".uniqid(),
                        'department_id'=>$this->departmentalizer($data['contribution']),
					'status'=>1
                	 );
//checks if description exists as array key and sets the variable or ignores it

if (array_key_exists("description",$data))
  {
  $payload['description']=$data['description'];
  }

// $this->response(array('status'=>'success','message'=>'Payment Updated','data'=>$payload));

// $this->load->model('receipts_model');
if($this->maine_model->insert('receipts',$payload)){
$this->response(array('status'=>true,'message'=>'Payment Updated'));

}else{
 $this->response(array('status'=>false,'message'=>'Unknown error'), Rest_controller::HTTP_INTERNAL_SERVER_ERROR);
}

}
else{

 $this->response(array('status'=>false,'message'=>validation_errors()), Rest_controller::HTTP_BAD_REQUEST);


                }

}


function departmentalizer($id){

//helper method for generating department id from contribution id
    $filter=array('id'=>$id);
    $dat=$this->maine_model->get_specific('contributions',$filter);
    // return $dat;
    if($dat!=null){
        return $dat[0]->did;
    }else{
        return 0;
    }
}






function contribution_report_post(){
    // gets contribution report based on  id of member and or department id and time limits

$data=$this->post();
$this->form_validation->set_data($data);
if($this->form_validation->run('report')!=false){
    $filters=array();
    if(isset($data['department_id'])){
        $filters['department_id']=$data['department_id'];
    }
    if(isset($data['member_id'])){
        $filters['member_id']=$data['member_id'];
    }
    if(isset($data['from'])){
        $filters['time_stamp >=']=date("Y-m-d h:i:sa",strtotime($data['from'].' 00:00:00'));
    }
    if(isset($data['to'])){
        $filters['time_stamp <=']=date("Y-m-d h:i:sa",strtotime($data['to'].' 23:59:59'));
    }
$all=$this->maine_model->get_specific('receipts',$filters);
if($all != null){
    $this->response(array('status'=>true,'message'=>$all));
}else{
    $this->response(array('status'=>false,'message'=>'No records'));
}

}else{

$this->response(array('status'=>false,'message'=>validation_errors()));

}

}







function expenditure_post(){
    //create a record of a new expenditure
$data=$this->post();
// echo json_encode($data);
$this->load->library('form_validation');
$this->form_validation->set_data($data);

                if($this->form_validation->run('expense')!=false ){
$payload=array('department_id' => $data['id'],
					'amount'=>$data['amount'],
						'description'=>$data['description'],
                        'code'=>"jkexp".uniqid(),
					'status'=>1
                	 );
$this->load->model('expenditure_model');
if($this->expenditure_model->insert($payload)){
$this->response(array('status'=>true,'message'=>'Expenditure Updated'));

}else{
 $this->response(array('status'=>false,'message'=>'Unknown error'), Rest_controller::HTTP_INTERNAL_SERVER_ERROR);
}

}
else{

  $this->response(array('status'=>false,'message'=>validation_errors()), Rest_controller::HTTP_BAD_REQUEST);


                }

}




function expenditure_report_post(){
$data=$this->post();
	$this->form_validation->set_data($data);
         if($this->form_validation->run('report')!=false ){
         	$filters=array();
    if(isset($data['department_id'])){
        $filters['department_id']=$data['department_id'];
    }
    if(isset($data['from'])){
        $filters['time_stamp >=']=date("Y-m-d h:i:sa",strtotime($data['from'].' 00:00:00'));
    }
    if(isset($data['to'])){
        $filters['time_stamp <=']=date("Y-m-d h:i:sa",strtotime($data['to'].' 23:59:59'));
    }
// $all=$this->maine_model->get_specific('expenditure',$filters);


         	$dat=$this->maine_model->get_specific('expenditure',$filters);
         	if($dat!=null){
         		 $this->response(array('status'=>true,'message'=>$dat));
         	}else{
         		 $this->response(array('status'=>false,'message'=>'No records'), Rest_controller::HTTP_BAD_REQUEST);

         	}
         	}else{

          $this->response(array('status'=>false,'message'=>validation_errors()), Rest_controller::HTTP_BAD_REQUEST);

         	}

}



function freddie_post(){

  $data=$this->post();
  $this->form_validation->set_data($data);
  $this->form_validation->set_rules('location', 'location', 'trim|xss_clean');
  $this->form_validation->set_rules('spaces', 'spaces', 'integer|required|trim');
  if($this->form_validation->run()!=false ){
    $payload = array();
    if (array_key_exists("spaces",$data))
     {
        $payload['spaces_left']=$data['spaces'];
     }
     if($this->maine_model->update('open',$payload,array('id'=>1))){
      $this->maine_model->firebase_2($this->maine_model->freddie_push());
    $this->response(array('status'=>true,'message'=>'spaces submitted'));
    }else{
        $this->response(array('status'=>false,'message'=>"Oops"), Rest_controller::HTTP_INTERNAL_SERVER_ERROR);

    }
  }else{
    $err = validation_errors();
    if($err == NULL){
      $err = "No data submitted";
    }
  $this->response(array('status'=>false,'message'=>$err), Rest_controller::HTTP_BAD_REQUEST);
  }


}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function open_parking_post(){

  $data=$this->post();

  $this->form_validation->set_data($data);
  $this->form_validation->set_rules('lat', 'lat', 'trim');
  $this->form_validation->set_rules('lng', 'lng', 'trim');
  $this->form_validation->set_rules('name', 'name', 'trim');

  if($this->form_validation->run()!=false ){
    $payload = array();

    if (array_key_exists("lat",$data) && array_key_exists("lng",$data))
     {
        $payload['coordinate']=array($data['lat'],$data['lng']);
     }
     if (array_key_exists("name",$data))
     {
        $payload['name']=$data['name'];
     }
    //  $this->response(array('status'=>true,'message'=>$payload['coordinate']));

     $datea = $this->maine_model->get_specific_f('open',$payload);
    //  $this->response(array('status'=>true,'message'=>$datea));
    //  if($datae != NULL){
    $this->response(array('status'=>true,'message'=>$datea));
    // }else{
    //   $this->response(array('status'=>false,'message'=>"No data available"), Rest_controller::HTTP_INTERNAL_SERVER_ERROR);
    //
    // }
  }else{
    $err = validation_errors();
    if($err == NULL){
      $err = $this->maine_model->get_plain2('open');
      $this->response(array('status'=>true,'message'=>$err));
    }
  $this->response(array('status'=>false,'message'=>$err), Rest_controller::HTTP_BAD_REQUEST);
}
}

}
