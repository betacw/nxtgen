<?php defined('BASEPATH') or exit ('No direct script access allowed');


$config = array(

        'payment' => array(
                array(
                        'field' => 'id',
                          'label' => 'id',
                            'rules' => 'trim|required'
                ),
                array(
                        'field' => 'contribution',
                        'label' => 'contribution',
                        'rules' => 'trim|required'
                ),
                array(
                        'field' => 'amount',
                        'label' => 'amount',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'description',
                        'label' => 'description',
                        'rules' => 'trim'
                ),
                 array(
                        'field' => 'member_type',
                        'label' => 'member_type',
                        'rules' => 'trim'
                )
        ),
        'expense' => array(
                array(
                        'field' => 'id',
                          'label' => 'id',
                            'rules' => 'trim|required'
                ),
                array(
                        'field' => 'amount',
                        'label' => 'amount',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'description',
                        'label' => 'description',
                        'rules' => 'required|trim'
                )
        ),
        //nxtgen
        'data' => array(
                array(
                        'field' => 'id',
                          'label' => 'id',
                            'rules' => 'trim|required|integer'
                ),
                array(
                        'field' => 'T',
                        'label' => 'T',
                        'rules' => 'trim|numeric'
                ),
                array(
                        'field' => 'H',
                        'label' => 'H',
                        'rules' => 'trim|numeric'
                ),
                array(
                        'field' => 'P',
                        'label' => 'P',
                        'rules' => 'trim|numeric'

                    )
        ),


         'filter' => array(
                array(
                        'field' => 'id',
                          'label' => 'id',
                            'rules' => 'trim|integer'
                ),
                array(
                        'field' => 'patient',
                        'label' => 'patient',
                        'rules' => 'trim|integer'
                ),
                array(
                        'field' => 'from',
                        'label' => 'from',
                        'rules' => 'trim'
                ),
                array(
                        'field' => 'to',
                        'label' => 'to',
                        'rules' => 'trim'

                    )
        ),
        'register' => array(
                array(
                        'field' => 'name',
                        'label' => 'name',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'phone',
                        'label' => 'phone',
                        'rules' => 'alpha_dash|min_length[9]|max_length[15]|callback_phone_check'
                        //make required when initial data is done
                ),
                array(
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'required|trim|valid_email|callback_email_check'
                ),
                array(
                        'field' => 'password',
                        'label' => 'password',
                        'rules' => 'trim|max_length[100]|xss_clean'
                ),

        ),


         'login' => array(
                
                array(
                        'field' => 'email',
                        'label' => 'email',
                        'rules' => 'required|trim|valid_email'
                ),
                array(
                        'field' => 'password',
                        'label' => 'password',
                        'rules' => 'required|trim|max_length[100]'
                )
                ),
         'report' => array(
                
                array(
                        'field' => 'department_id',
                        'label' => 'department_id',
                        'rules' => 'trim'
                ),
                array(
                        'field' => 'member_id',
                        'label' => 'member_id',
                        'rules' => 'trim'
                ),
                array(
                        'field'=>'from',
                        'label'=>'to',
                        'rules'=>'trim|min_length[4]'

                ),
                array(

                        'field'=>'to',
                        'label'=>'to',
                        'rules'=>'trim|min_length[4]'

                    )
                ),

          'wote' => array(
                array(
                        'field' => 'name',
                        'label' => 'name',
                        'rules' => 'required|trim'
                ),
                array(
                        'field' => 'data',
                        'label' => 'data',
                        'rules' => 'trim'
                ),
                array(
                        'field' => 'bed',
                        'label' => 'bed',
                        'rules' => 'trim'
                ),
                array(
                        'field' => 'ward',
                        'label' => 'ward',
                        'rules' => 'trim'
                ),
                array(
                        'field' => 'device',
                        'label' => 'device',
                        'rules' => 'trim|integer'
                    ),
        ),

          'edit' => array(
                array(
                        'field' => 'name',
                        'label' => 'name',
                        'rules' => 'trim'
                ),
                array(
                        'field' => 'device',
                        'label' => 'device',
                        'rules' => 'integer|callback_device_check'
                ),
                array(
                        'field' => 'bed',
                        'label' => 'bed',
                        'rules' => 'integer'
                ),
                array(
                        'field' => 'ward',
                        'label' => 'ward',
                        'rules' => 'integer'
                ),
                 array(
                        'field' => 'action',
                        'label' => 'action',
                        'rules' => 'integer|required'
                ),
                 array(
                        'field' => 'id',
                        'label' => 'id',
                        'rules' => 'integer|required'
                ),
                
        ),
          'lead' => array(
                array(
                        'field' => 'id',
                          'label' => 'id',
                            'rules' => 'trim|required'
                ),
                array(
                        'field' => 'department',
                        'label' => 'department',
                        'rules' => 'trim'
                ),
                array(
                        'field' => 'position',
                        'label' => 'position',
                        'rules' => 'trim'
                ),
                 array(
                        'field' => 'command',
                        'label' => 'command',
                        'rules' => 'trim'
                ),
                  array(
                        'field' => 'action',
                        'label' => 'action',
                        'rules' => 'trim'
                ),
                  array(
                        'field' => 'leader',
                        'label' => 'leader',
                        'rules' => 'trim'
                )
        
        )
);
